# 作业清单

![logo](screenshots/my_app_icon.png)

使用 ArkTS 开发的作业清单工具

#### 软件截图

![screenshot](screenshots/作业清单2.jpg)

#### 软件特色

- 美观、遵循 HarmonyOS 设计规范的 UX 设计，使用大量原生组件
- 支持任务名称、截止日期、完成情况分组查看的待办清单
- 支持用颜色、进度条指示距离截止日期的距离，管理待办时间一目了然

#### 安装教程

使用 DevEco Studio IDE 进行打包安装


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 致谢

- 本项目大量参考了 HarmonyOS 第一课及 Codelab 的开发知识
- UI 框架开发参考了 [OHOS Dev/OH12306](https://gitee.com/ohos-dev/oh12306)
- 开鸿派群友的帮助
- 我深夜掉的头发
